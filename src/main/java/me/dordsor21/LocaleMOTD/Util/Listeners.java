/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD.Util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.nbt.NbtBase;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import me.dordsor21.LocaleMOTD.Main;
import me.dordsor21.LocaleMOTD.Objects.MOTD;
import me.dordsor21.LocaleMOTD.Objects.MOTDList;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class Listeners implements Listener {

    private Main plugin;

    public Listeners(final Main pl, ProtocolManager pM) {
        this.plugin = pl;
        pM.addPacketListener(new PacketAdapter(pl, ListenerPriority.HIGHEST, PacketType.Play.Client.SETTINGS) {
            public void onPacketReceiving(PacketEvent e) {
                Player p = e.getPlayer();
                String locale = e.getPacket().getStrings().read(0).split("_")[0] + "_" + e.getPacket().getStrings().read(0).split("_")[1].toUpperCase();
                pl.localCache.put(e.getPlayer(), locale);
                pl.sql.setLocale(p, locale);
                if (e.getPlayer().hasMetadata("motd")) {
                    p.removeMetadata("motd", pl);
                    try {
                        MOTDList motdList;
                        if (pl.getMotds().containsKey(locale))
                            motdList = pl.getMotds().get(locale);
                        else if (pl.getMotds().containsKey(locale.split("_")[1].toUpperCase()))
                            motdList = pl.getMotds().get(locale.split("_")[1].toUpperCase());
                        else if (pl.getMotds().containsKey(locale.split("_")[0]))
                            motdList = pl.getMotds().get(locale.split("_")[0]);
                        else if (pl.getMotds().containsKey("default"))
                            motdList = pl.getMotds().get("default");
                        else
                            return;
                        MOTD motd = motdList.getMotdList().get(ThreadLocalRandom.current().nextInt(0, motdList.getMotdList().size()));
                        String m = motd.getMotd();
                        PacketPlayOutChat msg = new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(m));
                        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(msg);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        pM.addPacketListener(new PacketAdapter(pl, ListenerPriority.HIGHEST, PacketType.Play.Server.MAP_CHUNK, PacketType.Play.Server.TILE_ENTITY_DATA) {
            @Override
            public void onPacketSending(PacketEvent e) {
                if (e.getPacketType() == PacketType.Play.Server.MAP_CHUNK) {
                    List<NbtBase<?>> list = e.getPacket().getListNbtModifier().read(0);
                    Boolean packetEdited = false;
                    List<NbtBase<?>> list2 = new ArrayList<>();
                    for (NbtBase nbtB : list) {
                        NbtCompound nbt = (NbtCompound) nbtB;
                        if (nbt.getKeys().contains("id") && nbt.getString("id").equals("minecraft:sign")) {
                            Player p = e.getPlayer();
                            if (nbt.getString("Text1").startsWith("{\"extra\":[{\"text\":\"[") && nbt.getString("Text1").endsWith("]\"}],\"text\":\"\"}")
                                    && nbt.getString("Text2").equals("{\"text\":\"\"}") && nbt.getString("Text3").equals("{\"text\":\"\"}") && nbt.getString("Text4").equals("{\"text\":\"\"}")) {
                                String sign = nbt.getString("Text1").replace("{\"extra\":[{\"text\":\"[", "").replace("]\"}],\"text\":\"\"}", "");
                                if (pl.getSigns().containsKey(sign)) {
                                    Map<String, List<String>> locales = pl.getSigns().get(sign);
                                    String locale = null;
                                    if (pl.localCache.containsKey(p))
                                        locale = pl.localCache.get(p);
                                    else if (pl.cache)
                                        locale = pl.sql.getLocale(p);
                                    if (locale == null)
                                        locale = p.getLocale().split("_")[0] + "_" + p.getLocale().split("_")[1].toUpperCase();
                                    List<String> signs;
                                    if (locales.containsKey(locale))
                                        signs = locales.get(locale);
                                    else if (locales.containsKey(locale.split("_")[1].toUpperCase()))
                                        signs = locales.get(locale.split("_")[1].toUpperCase());
                                    else if (locales.containsKey(locale.split("_")[0]))
                                        signs = locales.get(locale.split("_")[0]);
                                    else if (locales.containsKey("default"))
                                        signs = locales.get("default");
                                    else
                                        return;
                                    String signString = signs.size() != 1 ? signs.get((int) (Math.random() * signs.size())) : signs.get(0);
                                    String[] lines = signString.split("\\\\n");
                                    for (int i = 0; i < 4; i++) {
                                        String line;
                                        String json;
                                        if (i < lines.length) {
                                            line = lines[i];
                                            json = Json.colourCodeToJson(line);
                                        } else {
                                            line = "~blank line~";
                                            json = "{\"text\":\"\"}";
                                        }
                                        try {
                                            packetEdited = true;
                                            pl.getLogger().severe("line: " + line);
                                            pl.getLogger().severe("json: " + json);
                                            nbt.put("Text" + String.valueOf(i + 1), json);
                                        } catch (Exception ex) {
                                            pl.getLogger().severe("Error Editing Sign Packet. Please report this to GitLab");
                                            pl.getLogger().severe("line: " + line);
                                            pl.getLogger().severe("json: " + json);
                                            ex.printStackTrace();
                                        }
                                    }
                                    list2.add(nbt);
                                }
                            }
                        }
                    }
                    if (packetEdited)
                        e.getPacket().getListNbtModifier().write(0, list2);
                }
                if (e.getPacketType() == PacketType.Play.Server.TILE_ENTITY_DATA) {
                    NbtCompound nbt = (NbtCompound) e.getPacket().getNbtModifier().read(0);
                    if (nbt.getKeys().contains("id") && nbt.getString("id").equals("minecraft:sign")) {
                        boolean packetEdited = false;
                        Player p = e.getPlayer();
                        if (nbt.getString("Text1").startsWith("{\"extra\":[{\"text\":\"[") && nbt.getString("Text1").endsWith("]\"}],\"text\":\"\"}")
                                && nbt.getString("Text2").equals("{\"text\":\"\"}") && nbt.getString("Text3").equals("{\"text\":\"\"}") && nbt.getString("Text4").equals("{\"text\":\"\"}")) {
                            String sign = nbt.getString("Text1").replace("{\"extra\":[{\"text\":\"[", "").replace("]\"}],\"text\":\"\"}", "");
                            if (pl.getSigns().containsKey(sign)) {
                                Map<String, List<String>> locales = pl.getSigns().get(sign);
                                String locale = null;
                                if (pl.localCache.containsKey(p))
                                    locale = pl.localCache.get(p);
                                else if (pl.cache)
                                    locale = pl.sql.getLocale(p);
                                if (locale == null)
                                    locale = p.getLocale().split("_")[0] + "_" + p.getLocale().split("_")[1].toUpperCase();
                                List<String> signs;
                                if (locales.containsKey(locale))
                                    signs = locales.get(locale);
                                else if (locales.containsKey(locale.split("_")[1].toUpperCase()))
                                    signs = locales.get(locale.split("_")[1].toUpperCase());
                                else if (locales.containsKey(locale.split("_")[0]))
                                    signs = locales.get(locale.split("_")[0]);
                                else if (locales.containsKey("default"))
                                    signs = locales.get("default");
                                else
                                    return;
                                String signString = signs.size() != 1 ? signs.get((int) (Math.random() * signs.size())) : signs.get(0);
                                String[] lines = signString.split("\\\\n");
                                for (int i = 0; i < 4; i++) {
                                    String line;
                                    String json;
                                    if (i < lines.length) {
                                        line = lines[i];
                                        json = Json.colourCodeToJson(line);
                                    } else {
                                        line = "~blank line~";
                                        json = "{\"text\":\"\"}";
                                    }
                                    try {
                                        packetEdited = true;
                                        pl.getLogger().severe("line: " + line);
                                        pl.getLogger().severe("json: " + json);
                                        nbt.put("Text" + String.valueOf(i + 1), json);
                                    } catch (Exception ex) {
                                        pl.getLogger().severe("Error Editing Sign Packet. Please report this to GitLab");
                                        pl.getLogger().severe("line: " + line);
                                        pl.getLogger().severe("json: " + json);
                                        ex.printStackTrace();
                                    }
                                }
                            }
                        }
                        if (packetEdited)
                            e.getPacket().getNbtModifier().write(0, nbt);
                    }
                }
            }
        });
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        e.getPlayer().setMetadata("motd", new FixedMetadataValue(plugin, true));
    }
}
