/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD.Util;

public class Json {

    public static String colourCodeToJson(String msg) {
        return "[{\"text\":\"" + msg
                .replace("&0", "\"},{\"color\":\"black\",\"text\":\"")
                .replace("&1", "\"},{\"color\":\"dark_blue\",\"text\":\"")
                .replace("&2", "\"},{\"color\":\"dark_green\",\"text\":\"")
                .replace("&3", "\"},{\"color\":\"dark_aqua\",\"text\":\"")
                .replace("&4", "\"},{\"color\":\"dark_red\",\"text\":\"")
                .replace("&5", "\"},{\"color\":\"dark_purple\",\"text\":\"")
                .replace("&6", "\"},{\"color\":\"gold\",\"text\":\"")
                .replace("&7", "\"},{\"color\":\"gray\",\"text\":\"")
                .replace("&8", "\"},{\"colo\":\"dark_gray\",\"text\":\"")
                .replace("&9", "\"},{\"color\":\"blue\",\"text\":\"")
                .replace("&a", "\"},{\"color\":\"green\",\"text\":\"")
                .replace("&b", "\"},{\"color\":\"aqua\",\"text\":\"")
                .replace("&c", "\"},{\"color\":\"red\",\"text\":\"")
                .replace("&d", "\"},{\"color\":\"light_purple\",\"text\":\"")
                .replace("&e", "\"},{\"color\":\"yellow\",\"text\":\"")
                .replace("&f", "\"},{\"color\":\"white\",\"text\":\"")
                .replace("&k", "\"},{\"obfuscated\":true,\"text\":\"")
                .replace("&l", "\"},{\"bold\":true,\"text\":\"")
                .replace("&m", "\"},{\"strikethrough\":true,\"text\":\"")
                .replace("&n", "\"},{\"underlined\":true,\"text\":\"")
                .replace("&o", "\"},{\"italic\":true,\"text\":\"")
                .replace("\"text\":\"\"},{", "")
                .replace("\"text\":\"\"},{", "") + "\"}]";
    }

}
