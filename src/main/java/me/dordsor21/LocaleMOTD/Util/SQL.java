/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD.Util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import me.dordsor21.LocaleMOTD.Main;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class SQL {

    private final static HashMap<String, String> columns;

    private String tableName;

    static {
        columns = new HashMap<>();
        columns.put("uuid", "VARCHAR(32)|PRIMARY|NOTNULL");
        columns.put("locale", "VARCHAR(16)NOTNULL");
    }

    private Connection conn;
    private Main plugin;

    public SQL(Main plugin) {
        this.plugin = plugin;
        tableName = plugin.getConfig().getString("SQL.tablePrefix", "") + "LocaleMOTD";
    }

    public boolean initialise() {
        try {
            MysqlDataSource source = new MysqlDataSource();
            source.setServerName(plugin.getConfig().getString("SQL.hostname"));
            source.setDatabaseName(plugin.getConfig().getString("SQL.database"));
            source.setUser(plugin.getConfig().getString("SQL.username"));
            source.setPassword(plugin.getConfig().getString("SQL.password"));
            source.setPort(plugin.getConfig().getInt("SQL.port"));
            source.setAutoReconnect(plugin.getConfig().getBoolean("SQL.autoreconnect", true));
            source.setUseSSL(plugin.getConfig().getBoolean("SQL.useSSL", false));
            source.setLoginTimeout(2);
            source.setMaxReconnects(1);
            conn = source.getConnection();
            tableExists();
            checkColumns();
            return true;
        } catch (SQLException e) {
            plugin.getLogger().severe("Error creating MySQL connection. Disabling persistence.");
            e.printStackTrace();
            return false;
        }
    }

    private void tableExists() {
        try {
            PreparedStatement stm = conn.prepareStatement("CREATE TABLE IF NOT EXISTS " + tableName + " (uuid VARCHAR(32) PRIMARY KEY NOT NULL, locale VARCHAR(16) NOT NULL)");
            stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void checkColumns() {
        try {
            for (String col : columns.keySet()) {
                PreparedStatement stmt1 = conn.prepareStatement("SELECT column_name FROM INFORMATION_SCHEMA.columns "
                        + "WHERE table_name = " + tableName + " AND column_name = '" + col + "';");
                ResultSet result = stmt1.executeQuery();
                if (!result.next()) {
                    String type = columns.get(col);
                    String def = "";
                    String auto_incr = "";
                    String nul = "";
                    String key = "";
                    if (type.contains("NOTNULL")) {
                        nul = " NOT NULL ";
                        type = type.replace("NOTNULL", "");
                    }
                    if (type.contains("AUTO_INCREMENT")) {
                        auto_incr = " AUTO_INCREMENT ";
                        type = type.replace("AUTO_INCREMENT", "");
                    }
                    if (type.contains("|")) {
                        key = " " + type.split("//|")[1] + " KEY ";
                        type = type.split("//|")[0] + type.split("//|")[2];
                    }
                    if (type.contains("!")) {
                        def = " DEFAULT " + type.split("!")[1].split("=")[1];
                        try {
                            type = type.split("!")[0] + type.split("!")[2];
                        } catch (ArrayIndexOutOfBoundsException e) {
                            type = type.split("!")[0].replace("!", "");
                        }
                    }
                    PreparedStatement stmt = conn.prepareStatement("ALTER TABLE " + tableName + " ADD COLUMN (" + col + " " + type + def + nul + auto_incr + key + ");");
                    stmt.executeUpdate();
                    plugin.getLogger().info("Column " + col + " created");
                    stmt.close();
                }
                stmt1.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getLocale(Player p) {
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT locale FROM " + tableName + " WHERE uuid=?");
            stmt.setString(1, p.getUniqueId().toString().replace("-", ""));
            ResultSet res = stmt.executeQuery();
            String ret = null;
            if (res.next())
                ret = res.getString("locale");
            stmt.close();
            res.close();
            return ret;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    void setLocale(Player p, String locale) {
        try {
            PreparedStatement stmt = conn.prepareStatement("REPLACE INTO " + tableName + " VALUES(?,?)");
            stmt.setString(1, p.getUniqueId().toString().replace("-", ""));
            stmt.setString(2, locale);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
