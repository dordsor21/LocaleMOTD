/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import lombok.Getter;
import me.dordsor21.LocaleMOTD.Objects.MOTDList;
import me.dordsor21.LocaleMOTD.Util.Listeners;
import me.dordsor21.LocaleMOTD.Util.SQL;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main extends JavaPlugin {

    @Getter
    private Map<String, MOTDList> motds = new HashMap<>();

    @Getter
    private Map<String, Map<String, List<String>>> signs = new HashMap<>();

    public Map<Player, String> localCache = new HashMap<>();

    public SQL sql;

    public boolean cache;

    @Override
    public void onEnable() {

        ProtocolManager pM = ProtocolLibrary.getProtocolManager();

        saveDefaultConfig();

        cache = getConfig().getBoolean("cacheLocale");
        if(cache) {
            sql = new SQL(this);
            cache = sql.initialise();
        }

        for (String locale : getConfig().getConfigurationSection("motd").getValues(false).keySet()) {
            motds.put(locale, new MOTDList(getConfig().getStringList("motd." + locale), pM));
        }

        for (String sign : getConfig().getConfigurationSection("signs").getValues(false).keySet()) {
            Map<String, List<String>> locales = new HashMap<>();
            getLogger().info(sign);
            for (String locale : getConfig().getConfigurationSection("signs." + sign).getValues(false).keySet()) {
                locales.put(locale, getConfig().getStringList("signs." + sign + "." + locale));
                getLogger().info(locale);
                getLogger().info(getConfig().getStringList("signs." + sign + "." + locale).toString());
            }
            signs.put(sign, locales);
        }

        getServer().getPluginManager().registerEvents(new Listeners(this, pM), this);
    }
}
