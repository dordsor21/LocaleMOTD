/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD.Objects;

import com.comphenix.protocol.ProtocolManager;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class MOTDList {

    @Getter
    private List<MOTD> motdList = new ArrayList<>();

    public MOTDList(List<String> list, ProtocolManager pM) {
        for (String motd : list) {
            motdList.add(new MOTD(motd, pM));
        }
    }
}
