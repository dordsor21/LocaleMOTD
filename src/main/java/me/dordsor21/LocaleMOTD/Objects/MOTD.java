/*
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 * Original by dordsor21 : https://gitlab.com/dordsor21/LocaleMOTD/blob/master/LICENSE
 */

package me.dordsor21.LocaleMOTD.Objects;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.ChatColor;

public class MOTD {

    @Getter
    private String motd;

    MOTD(String string, ProtocolManager pM) {
        String[] split = string.split(",,");
        int l = split.length;
        TextComponent tC = new TextComponent(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', split[0])));
        if (l > 1) {
            for (int i = 1; i < l; i = i + 2) {
                String event = split[i];
                String value = split[i + 1];
                if (event.startsWith("clickEvent")) {
                    String type = event.replace("clickEvent", "");
                    ClickEvent.Action action = ClickEvent.Action.valueOf(type.toUpperCase());
                    tC.setClickEvent(new ClickEvent(action, value));
                } else if (event.startsWith("hoverEvent")) {
                    String type = event.replace("hoverEvent", "");
                    HoverEvent.Action action = HoverEvent.Action.valueOf(type.toUpperCase());
                    tC.setHoverEvent(new HoverEvent(action, ComponentSerializer.parse(ComponentSerializer.toString(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', value))))));
                }
            }
        }
        String tCString = ComponentSerializer.toString(tC);
        WrappedChatComponent wCC = WrappedChatComponent.fromJson(tCString);
        PacketContainer pC = pM.createPacket(PacketType.Play.Server.CHAT);
        pC.getChatComponents().write(0, wCC);
        motd = pC.getChatComponents().read(0).getJson();
    }

}
